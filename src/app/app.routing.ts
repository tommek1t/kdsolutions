import {Routes} from "@angular/router";
import { AppComponent } from "./app.component";

export const APP_ROUTES: Routes = [
  {
    path: 'app',
    children: [
      {
        path: 'dashboard',
        component: AppComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/app/dashboard',
    pathMatch: 'full'
  }
];