import { Component, OnInit } from '@angular/core';
import { OneActivityComponent } from '../one-activity/one-activity.component';

@Component({
  selector: 'one-activity-payment',
  templateUrl: './one-activity-payment.component.html',
  styleUrls: ['./one-activity-payment.component.scss']
})
export class OneActivityPaymentComponent extends OneActivityComponent implements OnInit {

  constructor() {super(); }

  ngOnInit() {
  }

}
