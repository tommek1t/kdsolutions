import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneActivityPaymentComponent } from './one-activity-payment.component';

describe('OneActivityPaymentComponent', () => {
  let component: OneActivityPaymentComponent;
  let fixture: ComponentFixture<OneActivityPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneActivityPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneActivityPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
