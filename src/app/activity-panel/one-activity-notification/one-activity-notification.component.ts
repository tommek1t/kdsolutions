import { Component, OnInit, Input } from '@angular/core';
import { OneActivityComponent } from '../one-activity/one-activity.component';

@Component({
  selector: 'one-activity-notification',
  templateUrl: './one-activity-notification.component.html',
  styleUrls: ['./one-activity-notification.component.scss']
})
export class OneActivityNotificationComponent extends OneActivityComponent implements OnInit {
  @Input() notificationTime:String;

  constructor() {super(); }

  ngOnInit() {
  }

}
