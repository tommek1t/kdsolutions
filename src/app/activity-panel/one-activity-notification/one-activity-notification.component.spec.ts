import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneActivityNotificationComponent } from './one-activity-notification.component';

describe('OneActivityNotificationComponent', () => {
  let component: OneActivityNotificationComponent;
  let fixture: ComponentFixture<OneActivityNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneActivityNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneActivityNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
