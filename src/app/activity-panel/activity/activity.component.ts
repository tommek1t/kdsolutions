import { Component, OnInit } from '@angular/core';
import { TypeActivity } from '../../common/typeActivity';
import { Type } from '@angular/compiler/src/core';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  typeActivity=TypeActivity;
  constructor() { }

  ngOnInit() {
  }

}
