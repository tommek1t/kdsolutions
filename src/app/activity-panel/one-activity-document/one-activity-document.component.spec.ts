import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneActivityDocumentComponent } from './one-activity-document.component';

describe('OneActivityDocumentComponent', () => {
  let component: OneActivityDocumentComponent;
  let fixture: ComponentFixture<OneActivityDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneActivityDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneActivityDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
