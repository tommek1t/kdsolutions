import { Component, OnInit } from '@angular/core';
import { OneActivityComponent } from '../one-activity/one-activity.component';

@Component({
  selector: 'one-activity-document',
  templateUrl: './one-activity-document.component.html',
  styleUrls: ['./one-activity-document.component.scss']
})
export class OneActivityDocumentComponent extends OneActivityComponent implements OnInit {

  constructor() {super(); }

  ngOnInit() {
  }

}
