import { Component, OnInit, Input } from '@angular/core';
import { OneActivityComponent } from '../one-activity/one-activity.component';

@Component({
  selector: 'one-activity-individual',
  templateUrl: './one-activity-individual.component.html',
  styleUrls: ['./one-activity-individual.component.scss']
})
export class OneActivityIndividualComponent extends OneActivityComponent implements OnInit {
  @Input() avatarUrl: String;
  @Input() author: String;

  constructor() {super(); }

  ngOnInit() {
    this.avatarUrl = '../../assets/' + this.avatarUrl;
  }

}
