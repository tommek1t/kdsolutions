import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneActivityIndividualComponent } from './one-activity-individual.component';

describe('OneActivityIndividualComponent', () => {
  let component: OneActivityIndividualComponent;
  let fixture: ComponentFixture<OneActivityIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneActivityIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneActivityIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
