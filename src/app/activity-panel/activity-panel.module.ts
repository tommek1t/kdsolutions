import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityButtonComponent } from './activity-button/activity-button.component';
import { OneActivityPhotosComponent } from './one-activity-photos/one-activity-photos.component';
import { OneActivityNotificationComponent } from './one-activity-notification/one-activity-notification.component';
import { OneActivityPaymentComponent } from './one-activity-payment/one-activity-payment.component';
import { OneActivityDocumentComponent } from './one-activity-document/one-activity-document.component';
import { OneActivityIndividualComponent } from './one-activity-individual/one-activity-individual.component';
import { ActivityComponent } from './activity/activity.component';
import { OneActivityComponent } from './one-activity/one-activity.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ActivityComponent,
    OneActivityComponent,
    ActivityButtonComponent,
    OneActivityPhotosComponent,
    OneActivityNotificationComponent,
    OneActivityPaymentComponent,
    OneActivityDocumentComponent,
    OneActivityIndividualComponent
  ],
  exports: [ActivityComponent]
})
export class ActivityPanelModule { }
