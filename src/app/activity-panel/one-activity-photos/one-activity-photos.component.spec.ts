import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneActivityPhotosComponent } from './one-activity-photos.component';

describe('OneActivityPhotosComponent', () => {
  let component: OneActivityPhotosComponent;
  let fixture: ComponentFixture<OneActivityPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneActivityPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneActivityPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
