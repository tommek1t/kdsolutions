import { Component, OnInit, Input } from '@angular/core';
import { OneActivityComponent } from '../one-activity/one-activity.component';

@Component({
  selector: 'one-activity-photos',
  templateUrl: './one-activity-photos.component.html',
  styleUrls: ['./one-activity-photos.component.scss']
})
export class OneActivityPhotosComponent extends OneActivityComponent implements OnInit {
  @Input() avatarUrl: String;
  @Input() author: String;
  @Input() photoNames: String[] = ['home1.jpg', 'home2.jpg', 'home3.jpg', 'home4.png'];

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.avatarUrl) {
      this.avatarUrl = '../../assets/' + this.avatarUrl;
    }
    this.photoNames=this.photoNames.map(el => {return '../../assets/' + el});
  }

}
