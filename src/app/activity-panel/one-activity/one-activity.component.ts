import { Component, OnInit, Input, Output } from '@angular/core';
import { TypeActivity } from '../../common/typeActivity';

@Component({
  selector: 'app-one-activity',
  templateUrl: './one-activity.component.html',
  styleUrls: ['./one-activity.component.scss']
})
export class OneActivityComponent implements OnInit {
  @Input() title: String = "";
  @Input() describe: String = "";
  @Input() hashCode: String = "";
  @Input() date: String = "";
  @Input() type: TypeActivity;
  @Input() avatarUrl: String;
  @Input() author: String;
  typeActivity = TypeActivity;

  constructor() { }

  ngOnInit() {
    if (this.avatarUrl) {
      this.avatarUrl = '../../assets/' + this.avatarUrl;
    }
  }
}

