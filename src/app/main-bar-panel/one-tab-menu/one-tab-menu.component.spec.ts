import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneTabMenuComponent } from './one-tab-menu.component';

describe('OneTabMenuComponent', () => {
  let component: OneTabMenuComponent;
  let fixture: ComponentFixture<OneTabMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneTabMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneTabMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
