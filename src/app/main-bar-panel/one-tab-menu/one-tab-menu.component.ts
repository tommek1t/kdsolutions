import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'one-tab-menu',
  templateUrl: './one-tab-menu.component.html',
  styleUrls: ['./one-tab-menu.component.scss']
})
export class OneTabMenuComponent implements OnInit {
  @Input() notify:boolean=false;
  @Input() label:string="";
  @Input() clicked:boolean=false;

  constructor() { }

  ngOnInit() {
  }

}
