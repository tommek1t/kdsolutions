import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainBarComponent } from './main-bar/main-bar.component';
import { OneTabMenuComponent } from './one-tab-menu/one-tab-menu.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MainBarComponent,
    OneTabMenuComponent
  ],
  exports: [MainBarComponent]
})
export class MainBarPanelModule { }
