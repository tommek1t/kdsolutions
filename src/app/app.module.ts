import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {APP_ROUTES} from './app.routing';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ActivityPanelModule } from './activity-panel/activity-panel.module';
import { HomebuilderPanelModule } from './homebuilder-panel/homebuilder-panel.module';
import { MainBarPanelModule } from './main-bar-panel/main-bar-panel.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ActivityPanelModule,
    MainBarPanelModule,
    HomebuilderPanelModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
