import { Component, OnInit } from '@angular/core';
import { HomebuilderService } from '../services/homebuilder.service';

const maxPositions: number = 10;

@Component({
  selector: 'app-homebuilder',
  templateUrl: './homebuilder.component.html',
  styleUrls: ['./homebuilder.component.scss']
})
export class HomebuilderComponent implements OnInit {
  items: TodoItem[];
  positions: any[];
  constructor(private homeBuilderService: HomebuilderService) { }

  ngOnInit() {
    this.getListTodos();
  }

  setAsComplited(id: number) {
    this.homeBuilderService.setAsCompleted(id).subscribe(res => {
      this.getListTodos();
    });
  }

  setAsUncomplited(id: number) {
    this.homeBuilderService.setAsUncompleted(id).subscribe(res => {
      this.getListTodos();
    });
  }

  changeComplitedTaskStatus(item: TodoItem) {
    if (item.completed)
      this.setAsUncomplited(item.id);
    else this.setAsComplited(item.id);
  }

  private getListTodos() {
    this.homeBuilderService.getTodoItems().subscribe(items => {
      this.items = items
      this.positions = new Array(maxPositions - items.length);
  });
  }
}

export class TodoItem {
  id: number;
  todo: String;
  desc: String;
  info: String;
  completed: boolean;
  state: String;
}
