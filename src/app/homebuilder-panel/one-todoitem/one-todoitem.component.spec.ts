import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneTodoitemComponent } from './one-todoitem.component';

describe('OneTodoitemComponent', () => {
  let component: OneTodoitemComponent;
  let fixture: ComponentFixture<OneTodoitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneTodoitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneTodoitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
