import { Component, OnInit, Input } from '@angular/core';
import { TodoItem } from '../homebuilder/homebuilder.component';

@Component({
  selector: 'one-TODO-item',
  templateUrl: './one-todoitem.component.html',
  styleUrls: ['./one-todoitem.component.scss']
})
export class OneTodoitemComponent implements OnInit {
  @Input() item: TodoItem;
  @Input() avatarUrl: string='../../assets/Layla.png';

  constructor() { }

  ngOnInit() {
    switch (this.item.state) {
      case "high-price":
        this.item.state = "High Price";
        break;
      case "medium":
        this.item.state = "Medium";
        break;
      case "final":
        this.item.state = "Final";
        break;
    }
  }

}
