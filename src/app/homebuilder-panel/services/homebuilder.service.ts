import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { TodoItem } from '../homebuilder/homebuilder.component';

@Injectable()
export class HomebuilderService {
  private server: String = "http://localhost:4200/LRLHEVodbsDmVS0q/";

  constructor(private http: HttpClient) { }

  getTodoItems(): Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.server + 'todos');
  }

  setAsCompleted(id:number){
    return this.http.put(this.server+"todo/"+id+"/update/completed",{});
  }

  setAsUncompleted(id:number){
    return this.http.put(this.server+"todo/"+id+"/update/uncompleted",{});
  }

}
