import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomebuilderService } from './services/homebuilder.service';
import { OneTodoitemComponent } from './one-todoitem/one-todoitem.component';
import { HomebuilderComponent } from './homebuilder/homebuilder.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HomebuilderComponent,
    OneTodoitemComponent
  ],
  providers: [HomebuilderService],
  exports: [HomebuilderComponent]
})
export class HomebuilderPanelModule { }
